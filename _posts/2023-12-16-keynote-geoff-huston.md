---
layout: post
title: "Keynote announcement - Geoff Huston"
card: card-keynote-huston.png
---

<p class="lead">We are massively excited to announce our first Keynote speaker at Everything Open 2024, Geoff Huston!</p>

[Geoff Huston AM](https://en.wikipedia.org/wiki/Geoff_Huston_(internet)) is the [Chief Scientist at the Asia Pacific Network Information Centre (APNIC)](https://www.apnic.net/), where he undertakes research on topics associated with Internet infrastructure, internet protocol (IP) technologies, and address distribution policies.

Geoff will be presenting a keynote exploring how the Internet has changed in the five decades since its inception. It's bigger, faster, cheaper - but oh so different to what we imagined!

## About Geoff Huston

Geoff Huston played a critical role in bringing the Internet to Australia in the 1980s and 1990s. While the Internet was still in its infancy in the US, he was able to complete the construction of a new and rapidly growing network within a few months. In 1989 Geoff began work at the [Australian Vice Chancellor’s Committee](https://universitiesaustralia.edu.au/) with the direction to build a national academic and research network. In just over one year, every Australian university and major research institution was connected to the country’s first ISP, the [Australian Academic and Research Network](https://www.aarnet.edu.au/).

{% include keynote_image.html url="../../media/img/keynotes/geoff-huston.jpg" description="Image: Geoff Huston (supplied)" %}

The project quickly expanded to support the entire national Internet sector, and within five years this network became Australia’s largest private data network.

He has worked for the largest Australian communications service provider (Telstra) in senior engineering, architecture and research roles assisting with the large-scale deployment of the Internet across Australia and as a transit service provider in the Asia Pacific region.

He is [author of a number of Internet-related books](https://scholar.google.com/citations?hl=en&user=b7g6jncAAAAJ), and was a member of the [Internet Architecture Board](https://www.iab.org/) from 1999 until 2005, as well as chairing a number of Working Groups in the [Internet Engineering Task Force](https://www.ietf.org/). He served on the Board of Trustees of the [Internet Society](https://www.internetsociety.org/) from 1992 until 2001, and served as the chair of the Board in 1998 – 1999.

Geoff was inducted as an inaugural member of [The Internet Hall of Fame](https://www.internethalloffame.org/inductee/geoff-huston/) for his crucial work to get Australia online in the late 1980s, and in [2020 was made a member in the General Division of the Order of Australia (AM)](https://www.gg.gov.au/australian-honours-and-awardsaustralian-honours-lists/australia-day-2020-honours-list) for his role as an Internet pioneer in Australia.

## About the Keynote

Today's Internet is so different from what we had imagined it would be some forty years ago. It connects billions of devices, moves petabytes of data, it operates trunk circuits at terabits per second and the cost per delivered byte continues to drop. If this was the ultimate promise of Moore's Law and the silicon revolution, then the Internet is living that dream.

But it's not what we thought it would be. The rise of content distribution networks and the model of content and service replication has brought about massive changes to the Internet's architecture. By bringing service and content close to users, the network is bigger, faster and cheaper, but it's also completely different to the Internet we started with.

This presentation will explore these differences and what they mean to the future evolution of the Internet.

## Register now for Everything Open 2024!

[Register now](../../dashboard/) to attend Everything Open 2024, in beautiful Gladstone, tropical north Queensland. We have a range of [ticket prices](../../attend/tickets/) suitable for most budgets, and have compiled [information on travel to, and accommodation in Gladstone](../../attend/travel-accommodation/), as well as [information on what you can do while you're here](../../attend/gladstone/). We're looking forward to seeing you in April 2024!

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>
<a href="../../attend/tickets/" class="btn btn-secondary" role="button">View ticket prices</a>

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event. We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event. If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

<a href="../../sponsors/prospectus/" class="btn btn-primary" role="button">Sponsor Everything Open 2024</a>

