---
layout: post
title: "Early Bird Registration extended to 31 January"
card: card-announce-registrations-open.png
---

<p class="lead">We're delighted to announce that Early Bird Registration for Everything Open, Australia's premier open technologies conference, has been extended to 31 January, but will not be extended beyond this date.</p>

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>

Early Bird tickets offer deep discounts for committing to the conference early. 

Additionally, in recognition of both the current cost of living situation, and that Gladstone requires additional travel for most delegates, we have decided to keep ticket prices the same as Everything Open 2023. This would not be possible without our generous [sponsors](../../sponsors). 

Everything Open represents one of the best value for money, technically transdisciplinary conferences in Australia and Aotearoa New Zealand, bringing you industry luminaries, dozens of talks from experts, opportunities to make and renew connections with fellow practitioners, and invaluable insights into how everything open is powering the future.

<a href="../../attend/tickets/" class="btn btn-primary" role="button">View ticket prices</a>

Two incredible keynotes have already been announced - [internet luminary Geoff Huston AM](../keynote-geoff-huston/), and [cyber threat analyst Jana Dekanovska](../keynote-jana-dekanovska/) - and we have two more yet to announce!

[Register now](../../dashboard/) to attend Everything Open 2024, in beautiful Gladstone, tropical north Queensland. We have a range of [ticket prices](../../attend/tickets/) suitable for most budgets, and have compiled [information on travel to, and accommodation in Gladstone](../../attend/travel-accommodation/), as well as [information on what you can do while you're here](../../attend/gladstone/). While Everything Open is reason enough to get to Gladstone in April, we’ve also [curated information on other activities you might like to partake in as part of your trip](../../attend/gladstone/) - including the stunning [Heron Island](https://www.heronisland.com/). We also have all of your [travel and accommodation information covered here](../../attend/travel-accommodation/).

So, join the region’s open source, open data, open GLAM, open gov, open science, open hardware and other open practitioners, and power up today! We're looking forward to seeing you in April 2024. 

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event.

We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).











