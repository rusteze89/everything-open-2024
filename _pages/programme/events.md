---
layout: page
title: Events
permalink: /programme/events/
sponsors: true
---

During Everything Open we will be running some events in addition to the talks and tutorials.
Further details of the events will be published at a later date.

<a id="penguin-dinner"></a>
## Penguin Dinner

Held one evening during the conference, the Penguin Dinner is the main social event of the conference.

_For Penguin Dinner ticket holders only._
