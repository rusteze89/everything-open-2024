---
layout: page
title: Contact
permalink: /about/contact/
sponsors: true
---

## Contact the Organising Team

Is there something you would like to know that isn't on this site? Please contact us via email.

<i class="bi-envelope-fill"></i> Email: [contact@everythingopen.au](mailto:contact@everythingopen.au)

## Safety

Everything Open aims to be an inclusive event which welcomes diverse groups from all parts of the open technologies community to an environment of respect, tolerance, and encouragement.
If you see or experience inappropriate or exclusionary behaviour, please get in touch with our safety team.

<i class="bi-envelope-fill"></i> Email: [safety@everythingopen.au](mailto:safety@everythingopen.au)

## Stay up to date

We have several channels that we use to post announcements in the lead up to, and during, the conference.

{% include socials.html %}

## Contact Linux Australia

Linux Australia is the auspice for Everything Open.
Check out their website for more details and contact information [linux.org.au](https://linux.org.au)
