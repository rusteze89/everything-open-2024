---
layout: page
title: About
permalink: /about/
sponsors: true
---

## Important Information

Everything Open 2024 will be held at the [Gladstone Exhibition Convention Centre](/about/location/) from April 16-18 2024.
The conference will be run in the Australian Eastern Standard Time (AEST - UTC+10) timezone.

## About Everything Open

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The presentations cover a broad range of subject areas, including Linux, open source software, open hardware, open data, open government, open GLAM (galleries, libraries, archives and museums), to name a few.
There are technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.
The conference draws upon the experience of the many events that have been run by Linux Australia and its subcommittees, starting with CALU (Conference of Australian Linux Users) in 1999, linux.conf.au over the past twenty years, and the Open Source Developers Conference (OSDC).

At the core of Everything Open is the community.
The conference is entirely organised by volunteers who have a passion for bringing together the open technologies communities to share their collective experience.
Everything Open is a not for profit event that aims to provide attendees with a world-class conference at an affordable rate.

## Linux Australia

[Linux Australia](https://linux.org.au) represents Australian users and developers of Free Software and Open Technologies, and facilitates internationally-renowned events.
Linux Australia provide the financial, insurance, and technical infrastructure for Everything Open.

## Our Team

Everything Open 2024 is organised by a core team of volunteers, lead by Rob Thomas, who contribute many hours of their time to run this event.
In addition to this core team there are many other contributors who put in significant effort to make this a successful event.
