## Everything Open 2024 website

https://2024.everythingopen.au

## Development

This site uses [jekyll](https://jekyllrb.com/), a Ruby-based static website generator

## Local environment

``` shell
gem install jekyll
jekyll serve -w
```
